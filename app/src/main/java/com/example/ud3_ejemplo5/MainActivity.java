package com.example.ud3_ejemplo5;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Los datos devueltos tendrán como nombre asociado el valor de la constante.
    public static final String DATO_DEVUELTO = "DATO_DEVUELTO";

    // Constante para el valor por defecto del método getIntExtra del Intent.
    private static final int VALOR_DEFECTO = 0;

    /* Preparamos la respuesta de los Intents para ello registramos la respuesta de la llamada haciendo uso
       del método registerForActivityResult.
       La clase ActivityResultContracts nos permite utilizar una serie de clases útiles como capturar vídeo o
       abrir documentos.
       El método onActivityResult del callback ActivityResultCallback nos devuelve la respuesta al Intent.
     */
    ActivityResultLauncher<Intent> resultAct1 = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    // Comprobamos que el código de respuesta es correcto
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // Obtenemos el Intent devuelto y mostramos su contenido
                        Intent datosResult = result.getData();

                        Toast.makeText(MainActivity.this, datosResult.getStringExtra(DATO_DEVUELTO), Toast.LENGTH_LONG).show();
                    }
                }
            });

    ActivityResultLauncher<Intent> resultAct2 = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    // Comprobamos que el código de respuesta es correcto
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // Obtenemos el Intent devuelto y mostramos su contenido
                        Intent datosResult = result.getData();

                        Toast.makeText(MainActivity.this, Integer.toString(datosResult.getIntExtra(DATO_DEVUELTO, VALOR_DEFECTO)), Toast.LENGTH_LONG).show();
                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Buscamos el TextView de la actividad 1 por su id (actividad1).
        TextView actividad1 = findViewById(R.id.actividad1);

        // Asignamos un Click Listener sobre el TextView
        actividad1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAct1 = new Intent(MainActivity.this, Actividad1.class);

                // Añadimos los datos extra para pasárselos a la actividad
                intentAct1.putExtra(Intent.EXTRA_TEXT,"Texto Actividad 1");

                // Lanzamos el Intent
                resultAct1.launch(intentAct1);
            }
        });

        // Buscamos el TextView de la actividad 2 por su id (actividad2).
        TextView actividad2 = findViewById(R.id.actividad2);

        // Asignamos un Click Listener sobre el TextView
        actividad2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAct2 = new Intent(MainActivity.this, Actividad2.class);

                // Añadimos los datos extra para pasárselos a la actividad
                intentAct2.putExtra(Intent.EXTRA_TEXT,"Texto Actividad 2");

                // Lanzamos el Intent
                resultAct2.launch(intentAct2);
            }
        });
    }
}